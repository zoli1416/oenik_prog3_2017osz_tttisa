﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Create Bindable class for notify if properties changed
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Eventhandler when a property changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Function for property changed
        /// </summary>
        /// <param name="name">Defines the name of the caller</param>
        protected void OnPropertyChanged(string name = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Function to set field value and fire event
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="field">The field what was  changed</param>
        /// <param name="value">The field new value</param>
        /// <param name="name">The function caller name</param>
        protected void SetProperty<T>(ref T field, T value, [CallerMemberName] string name = null)
        {
            field = value;
            this.OnPropertyChanged(name);
        }
    }
}
