﻿// <copyright file="FallingObjectVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for falling object selection
    /// </summary>
    public class FallingObjectVM
    {
        /// <summary>
        /// Gets or sets x coordinate of the object
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets y coordinate of the object
        /// </summary>
        public int Y { get; set; }
    }
}
