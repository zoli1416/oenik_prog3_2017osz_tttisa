﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Timers;
    using System.Windows;

    /// <summary>
    /// Game functionality / game logic
    /// </summary>
    public class GameLogic : Bindable
    {
        /// <summary>
        /// Private model
        /// </summary>
        private GameModel model;

        /// <summary>
        /// Falling Energy type objects => we have them fall with timer
        /// </summary>
        private List<FallingObjectVM> fallingEnergyObjects;

        /// <summary>
        /// It is the same like at the Energy with the exception that can make an explosion.
        /// Explosion is made when collides with anything but wall
        /// </summary>
        private List<FallingObjectVM> fallingBombsObjects;

        /// <summary>
        /// If we have 3 energy balls then one ball contains 33.33% energy (we round up at the end > 98 )
        /// </summary>
        private int energyPerUnit;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// Import elements from level files usually with bool arrays or Point structures
        /// We get the map size
        /// We set the maximum livesleft parameter
        /// Map has unique meanings:
        /// First to rows correspond for the size
        /// Third one is for the quantity of energy balls needed to pass the level
        /// Fourth row is the time limit for the map
        /// Items written like this:
        /// e - wall ; i - rock; d - dust(sand); K - key; G - gate(door); energy ball - o
        /// </summary>
        /// <param name="fname">File name for the map</param>
        public GameLogic(string fname)
        {
            this.model = new GameModel();
            this.model.LivesLeft = 3;
            string[] lines = File.ReadAllLines(fname);
            int xsize = int.Parse(lines[0]);
            int ysize = int.Parse(lines[1]);
            this.energyPerUnit = int.Parse(lines[2]);
            this.model.TimeLeft = int.Parse(lines[3]);
            this.model.Walls = new bool[xsize, ysize];
            this.model.RockWalls = new bool[xsize, ysize];
            this.model.Sands = new bool[xsize, ysize];
            this.model.EnergyBall = new bool[xsize, ysize];
            this.model.Bombs = new bool[xsize, ysize];
            for (int x = 0; x < xsize; x++)
            {
                for (int y = 0; y < ysize; y++)
                {
                    this.model.Walls[x, y] = lines[y + 4][x] == 'e';
                    this.model.RockWalls[x, y] = lines[y + 4][x] == 'i';
                    this.model.Sands[x, y] = lines[y + 4][x] == 'd';
                    this.model.EnergyBall[x, y] = lines[y + 4][x] == 'o';
                    this.model.Bombs[x, y] = lines[y + 4][x] == 'b';
                    if (lines[y + 4][x] == 'S')
                    {
                        this.model.Player = new Point(x, y);
                    }

                    if (lines[y + 4][x] == 'F')
                    {
                        this.model.Exit = new Point(x, y);
                    }

                    if (lines[y + 4][x] == 'G')
                    {
                        this.model.Door = new Point(x, y);
                    }

                    if (lines[y + 4][x] == 'K')
                    {
                        this.model.Key = new Point(x, y);
                    }
                }
            }
        }

        /// <summary>
        /// Gets gameModel
        /// </summary>
        public GameModel Model
        {
            get { return this.model; }
            private set { this.SetProperty(ref this.model, value); }
        }

        /// <summary>
        /// Function to move around the map
        /// </summary>
        /// <param name="dx">x coordinate</param>
        /// <param name="dy">y coordinate</param>
        /// <returns>True if we got to end point</returns>
        public bool Move(int dx, int dy)
        {
            // These are the coordinates we would like to go so we analyze them can we?
            int newx = (int)(this.model.Player.X + dx);
            int newy = (int)(this.model.Player.Y + dy);

            // If we get a key it will open the door and both disapears
            if (this.model.Key.HasValue && newx == this.model.Key.Value.X && newy == this.model.Key.Value.Y)
            {
                this.model.Key = null;
                this.model.Door = null;
            }

            // Move the energyball very carefully because we cannot move it if something is in the way
            // dx has to be greater then 0 in order to go right or left (these are the only possibilities to move the energy ball)
            // After that we analyze if the movement it is still in the map sorrounded by walls
            // We check if anything obstructs the energy balls way if not we move
            if (dx != 0 && newx >= 0
                && newy >= 0
                && this.model.EnergyBall[newx, newy]
                && (newx + dx) < this.model.Walls.GetLength(0)
                && (newy + dy) < this.model.Walls.GetLength(1)
                && (!this.model.Walls[newx + dx, newy + dy]
                && !this.model.RockWalls[newx + dx, newy + dy] && !this.model.EnergyBall[newx + dx, newy + dy] && !this.model.Sands[newx + dx, newy + dy] && (!this.model.Door.HasValue || (this.model.Door.Value.X != newx + dx || this.model.Door.Value.Y != newy + dy))))
            {
                // Here we make a movement with the energy ball
                bool[,] temp = new bool[this.model.EnergyBall.GetLength(0), this.model.EnergyBall.GetLength(1)];
                temp = this.model.EnergyBall;
                temp[newx, newy] = false;
                temp[newx + dx, newy + dy] = true;
                this.model.EnergyBall = temp;
            }

            // Move the bomb is very similar to the energy ball only exception is that when the bomb falls it can explode
            if (dx != 0 && newx >= 0
                && newy >= 0
                && this.model.Bombs[newx, newy]
                && (newx + dx) < this.model.Walls.GetLength(0)
                && (newy + dy) < this.model.Walls.GetLength(1)
                && (!this.model.Walls[newx + dx, newy + dy]
                && !this.model.RockWalls[newx + dx, newy + dy] && !this.model.Bombs[newx + dx, newy + dy]))
            {
                bool[,] temp = new bool[this.model.Bombs.GetLength(0), this.model.Bombs.GetLength(1)];
                temp = this.model.Bombs;
                temp[newx, newy] = false;
                temp[newx + dx, newy + dy] = true;
                this.model.Bombs = temp;
            }

            // Analyze the player movement and the positions with sand
            // (if the player moves to a location where is sand it will clear it off)
            if (newx >= 0
                && newy >= 0
                && newx < this.model.Walls.GetLength(0)
                && newy < this.model.Walls.GetLength(1)
                && (!this.model.Walls[newx, newy] && !this.model.RockWalls[newx, newy] && !this.model.EnergyBall[newx, newy] && (!this.model.Door.HasValue || this.model.Door.Value.X != newx || this.model.Door.Value.Y != newy) && (this.model.EndEnergy == 100 || this.model.Exit.X != newx || this.model.Exit.Y != newy)))
            {
                // We move the player here
                this.model.Player = new Point(newx, newy);

                // If there is sand we clean
                if (this.model.Sands[newx, newy])
                {
                    bool[,] temp = new bool[this.model.Sands.GetLength(0), this.model.Sands.GetLength(1)];
                    temp = this.model.Sands;
                    temp[newx, newy] = false;
                    this.model.Sands = temp;
                }
            }

            // If the player reached the exit point and gained all the energy necessary then the map is completed
            return this.model.Player.Equals(this.model.Exit) && this.model.EndEnergy == 100;
        }

        /// <summary>
        /// Obtain every object which could fall down after movement
        /// We watch them constantly and a separated timer makes the animation likeness
        /// </summary>
        public void FindFallingObjects()
        {
            // There are different types like energy balls and bombs so we get them separately
            this.fallingEnergyObjects = new List<FallingObjectVM>();
            this.fallingBombsObjects = new List<FallingObjectVM>();

            // First we obtain the energy balls
            for (int x = 0; x < this.model.EnergyBall.GetLength(0); x++)
            {
                for (int y = 0; y < this.model.EnergyBall.GetLength(1); y++)
                {
                    if (this.model.EnergyBall[x, y])
                    {
                        this.fallingEnergyObjects.Add(new FallingObjectVM { X = x, Y = y });
                    }
                }
            }

            // Then we obtain the bombs
            for (int x = 0; x < this.model.Bombs.GetLength(0); x++)
            {
                for (int y = 0; y < this.model.Bombs.GetLength(1); y++)
                {
                    if (this.model.Bombs[x, y])
                    {
                        this.fallingBombsObjects.Add(new FallingObjectVM { X = x, Y = y });
                    }
                }
            }

            // After we have every falling object we can iterate through them and check if they can fall
            // An object can fall if there is no dust, rock, wall, player, bomb underneath
            foreach (FallingObjectVM item in this.fallingEnergyObjects)
            {
                this.FallingEnergy(item.X, item.Y);
            }

            foreach (FallingObjectVM item in this.fallingBombsObjects)
            {
                this.FallingBombs(item.X, item.Y);
            }
        }

        /// <summary>
        /// Set remaining lives after dying (decrease)
        /// </summary>
        public void Died()
        {
            this.model.LivesLeft--;
        }

        /// <summary>
        /// Set remaining time with every thick (decrease)
        /// </summary>
        public void TimeLeft()
        {
            this.model.TimeLeft--;
        }

        /// <summary>
        /// Separate function to simulate how energy ball should fall
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        private void FallingEnergy(int x, int y)
        {
            int lastx = x;
            int lasty = y;
            bool[,] temp = this.model.EnergyBall;
            if (this.CanFallDown(x, y + 1))
            {
                temp[lastx, lasty] = false;
                if (this.model.Exit.X != x || this.model.Exit.Y != y + 1)
                {
                    temp[x, y + 1] = true;
                }
                else
                {
                    this.model.EndEnergy += 100 / this.energyPerUnit;
                    if (this.model.EndEnergy > 98 && this.model.EndEnergy < 100)
                    {
                        this.model.EndEnergy = 100;
                    }
                }

                y++;
                lasty = y;
                this.model.EnergyBall = temp;
            }
        }

        /// <summary>
        /// Separate function to simulate how bombs should fall and potencially explode
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        private void FallingBombs(int x, int y)
        {
            // First the bomb will fall like an energy ball
            int lastx = x;
            int lasty = y;
            bool[,] temp = this.model.Bombs;
            if (this.CanBombFallDown(x, y + 1))
            {
                temp[lastx, lasty] = false;
                if (this.model.Exit.X != x || this.model.Exit.Y != y + 1)
                {
                    temp[x, y + 1] = true;
                }

                y++;
                lasty = y;
                this.model.Bombs = temp;
            } // If the bomb reached the bottom then it means we need to analyze if it is going to explode or not
            else if (this.CanBombDetonate(x, y + 1))
            {
                // If the bomb lands on rock surface it will explode in one radius in every direction
                bool[,] rocktemp = new bool[this.model.RockWalls.GetLength(0), this.model.RockWalls.GetLength(1)];
                rocktemp = this.model.RockWalls;

                // We analyze every direction separately and if it is in the range of the bomb we delete it from the map
                if (x < this.model.RockWalls.GetLength(0) && y + 1 < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x, y + 1])
                {
                    rocktemp[x, y + 1] = false;
                }

                if (x < this.model.RockWalls.GetLength(0) && y < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x, y])
                {
                    rocktemp[x, y] = false;
                }

                if (x - 1 < this.model.RockWalls.GetLength(0) && y < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x - 1, y])
                {
                    rocktemp[x - 1, y] = false;
                }

                if (x - 1 < this.model.RockWalls.GetLength(0) && y + 1 < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x - 1, y + 1])
                {
                    rocktemp[x - 1, y + 1] = false;
                }

                if (x - 1 < this.model.RockWalls.GetLength(0) && y + 2 < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x - 1, y + 2])
                {
                    rocktemp[x - 1, y + 2] = false;
                }

                if (x < this.model.RockWalls.GetLength(0) && y + 2 < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x, y + 2])
                {
                    rocktemp[x, y + 2] = false;
                }

                if (x + 1 < this.model.RockWalls.GetLength(0) && y + 2 < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x + 1, y + 2])
                {
                    rocktemp[x + 1, y + 2] = false;
                }

                if (x + 1 < this.model.RockWalls.GetLength(0) && y + 1 < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x + 1, y + 1])
                {
                    rocktemp[x + 1, y + 1] = false;
                }

                if (x + 1 < this.model.RockWalls.GetLength(0) && y < this.model.RockWalls.GetLength(1) && this.model.RockWalls[x + 1, y])
                {
                    rocktemp[x + 1, y] = false;
                }

                this.model.RockWalls = rocktemp;

                // the bomb exploded so lets take of also the bomb off the map
                bool[,] bombtemp = new bool[this.model.Bombs.GetLength(0), this.model.Bombs.GetLength(1)];
                bombtemp = this.model.Bombs;
                bombtemp[x, y] = false;
                this.model.Bombs = bombtemp;
            }
        }

        /// <summary>
        /// If there is dust, rock, wall under the bomb should not fall
        /// Otherwise if these circumstances will change it may fall and even explode
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        /// <returns>boolean if it's true we can move the bomb down</returns>
        private bool CanBombFallDown(int x, int y)
        {
            return !this.model.Walls[x, y] && !this.model.RockWalls[x, y] && !this.model.Sands[x, y] && !this.model.EnergyBall[x, y] && (this.model.Player.X != x || this.model.Player.Y != y);
        }

        /// <summary>
        /// Similar to the bomb only these are for other object (like energy balls)
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        /// <returns>boolean if it's true we can move the object down (simulate a fall)</returns>
        private bool CanFallDown(int x, int y)
        {
            return !this.model.Walls[x, y] && !this.model.RockWalls[x, y] && !this.model.Sands[x, y] && !this.model.EnergyBall[x, y] && !this.model.Bombs[x, y] && (this.model.Player.X != x || this.model.Player.Y != y);
        }

        /// <summary>
        /// If the bomb landed on a surface it can destroy it will detonate
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        /// <returns>boolean if it's true the bomb will explode</returns>
        private bool CanBombDetonate(int x, int y)
        {
            return this.model.RockWalls[x, y] || this.model.Sands[x, y] || this.model.EnergyBall[x, y] || (this.model.Player.X == x || this.model.Player.Y == y);
        }
    }
}
