﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// GameModel
    /// </summary>
    public class GameModel : Bindable
    {
        /// <summary>
        /// One tile width
        /// </summary>
        public const int Tilew = 40;

        /// <summary>
        /// One tile height
        /// </summary>
        public const int Tileh = 40;

        /// <summary>
        /// How many chances left
        /// </summary>
        private int livesLeft;

        /// <summary>
        /// How many seconds left
        /// </summary>
        private int timeLeft;

        /// <summary>
        /// Score -- from what time was left when finished the map
        /// </summary>
        private int score;

        /// <summary>
        /// Bool array if true it is a wall
        /// </summary>
        private bool[,] walls;

        /// <summary>
        /// Bool array if true it is a rock wall
        /// </summary>
        private bool[,] rockwalls;

        /// <summary>
        /// Bool array if true it is sand
        /// </summary>
        private bool[,] sands;

        /// <summary>
        /// Bool array if true it is an energyball
        /// </summary>
        private bool[,] energyball;

        /// <summary>
        /// Bool array if true there is a bomb waiting to explode
        /// </summary>
        private bool[,] bombs;

        /// <summary>
        /// Percentege of game finished
        /// </summary>
        private int endEnergy;

        /// <summary>
        /// Number of map level
        /// </summary>
        private int currentLevel;

        /// <summary>
        /// Representing player
        /// </summary>
        private Point player;

        /// <summary>
        /// Representing one key
        /// </summary>
        private Point? key;

        /// <summary>
        /// Representing one door
        /// </summary>
        private Point? door;

        /// <summary>
        /// Representing exit
        /// </summary>
        private Point exit;

        /// <summary>
        /// Gets this object
        /// </summary>
        public GameModel Self
        {
            get { return this; }
        }

        /// <summary>
        /// Gets or sets lifes left property
        /// </summary>
        public int LivesLeft
        {
            get
            {
                return this.livesLeft;
            }

            set
            {
                this.SetProperty(ref this.livesLeft, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets lifes left property
        /// </summary>
        public int TimeLeft
        {
            get
            {
                return this.timeLeft;
            }

            set
            {
                this.SetProperty(ref this.timeLeft, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets score
        /// </summary>
        public int Score
        {
            get
            {
                return this.score;
            }

            set
            {
                this.SetProperty(ref this.score, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets walls bool array
        /// </summary>
        public bool[,] Walls
        {
            get
            {
                return this.walls;
            }

            set
            {
                this.SetProperty(ref this.walls, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets rock walls bool array
        /// </summary>
        public bool[,] RockWalls
        {
            get
            {
                return this.rockwalls;
            }

            set
            {
                this.SetProperty(ref this.rockwalls, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets sands bool array
        /// </summary>
        public bool[,] Sands
        {
            get
            {
                return this.sands;
            }

            set
            {
                this.SetProperty(ref this.sands, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets energy ball bool array
        /// </summary>
        public bool[,] EnergyBall
        {
            get
            {
                return this.energyball;
            }

            set
            {
                this.SetProperty(ref this.energyball, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets bombs bool array (represents where are bombs)
        /// </summary>
        public bool[,] Bombs
        {
            get
            {
                return this.bombs;
            }

            set
            {
                this.SetProperty(ref this.bombs, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets Player point
        /// </summary>
        public Point Player
        {
            get
            {
                return this.player;
            }

            set
            {
                this.SetProperty(ref this.player, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets key point
        /// </summary>
        public Point? Key
        {
            get
            {
                return this.key;
            }

            set
            {
                if (value.HasValue)
                {
                    this.SetProperty(ref this.key, value.Value);
                }
                else
                {
                    this.SetProperty(ref this.key, null);
                }

                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets door point
        /// </summary>
        public Point? Door
        {
            get
            {
                return this.door;
            }

            set
            {
                if (value.HasValue)
                {
                    this.SetProperty(ref this.door, value.Value);
                }
                else
                {
                    this.SetProperty(ref this.door, null);
                }

                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets endEnergy value
        /// </summary>
        public int EndEnergy
        {
            get
            {
                return this.endEnergy;
            }

            set
            {
                this.SetProperty(ref this.endEnergy, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets current level
        /// </summary>
        public int CurrentLevel
        {
            get
            {
                return this.currentLevel;
            }

            set
            {
                this.SetProperty(ref this.currentLevel, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets Exit point
        /// </summary>
        public Point Exit
        {
            get
            {
                return this.exit;
            }

            set
            {
                this.SetProperty(ref this.exit, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets player brush, an image
        /// </summary>
        public Brush PlayerBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/carter.png");
            }
        }

        /// <summary>
        /// Gets key brush, an image
        /// </summary>
        public Brush KeyBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/key.png");
            }
        }

        /// <summary>
        /// Gets door brush, an image
        /// </summary>
        public Brush DoorBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/door.png");
            }
        }

        /// <summary>
        /// Gets exit brush, an image
        /// </summary>
        public Brush ExitBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/exit.png");
            }
        }

        /// <summary>
        /// Gets wall brush, an image
        /// </summary>
        public Brush WallBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/wall.png");
            }
        }

        /// <summary>
        /// Gets rock brush, an image
        /// </summary>
        public Brush RockWallBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/rock.png");
            }
        }

        /// <summary>
        /// Gets sand brush, an image
        /// </summary>
        public Brush SandsBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/sand.png");
            }
        }

        /// <summary>
        /// Gets energyball brush, an image
        /// </summary>
        public Brush EnergyBallBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/energyball.png");
            }
        }

        /// <summary>
        /// Gets bomb brush, an image
        /// </summary>
        public Brush BombBrush
        {
            get
            {
                return this.GetBrush("pack://application:,,,/Images/bomb.png");
            }
        }

        /// <summary>
        /// Get image from file
        /// </summary>
        /// <param name="fname">File name</param>
        /// <returns>Image converted to Brush</returns>
        public Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fname, UriKind.Absolute)));
            ib.TileMode = TileMode.Tile; // sets to tile mode
            ib.Viewport = new Rect(0, 0, Tilew, Tileh); // vs ViewBox
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
