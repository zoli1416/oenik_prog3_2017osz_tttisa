﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameLogic logic;
        private Stopwatch stw;
        private DispatcherTimer timer = new DispatcherTimer();
        private DispatcherTimer timerForCountDown = new DispatcherTimer();
        private int oldLives;
        private int score;
        private string[] levelArray;
        private int currentLevel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            // we get every level file, dynamic
            this.GetLevels();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.logic = new GameLogic("Levels/" + this.levelArray[this.currentLevel - 1]);
            this.logic.Model.CurrentLevel = this.currentLevel;
            this.DataContext = this.logic.Model;
            this.timer.Interval = new System.TimeSpan(0, 0, 0, 0, 100);
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();

            this.timerForCountDown.Interval = new System.TimeSpan(0, 0, 0, 1, 0);
            this.timerForCountDown.Tick += this.TimerForCountDown_Tick;
            this.timerForCountDown.Start();

            this.stw = new Stopwatch();
            this.stw.Start();
        }

        private void GetLevels()
        {
            // Read every filename from folder Levels to get maps
            this.currentLevel = 1;
            string projectFolder = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName;
            string folderAppData = Path.Combine(projectFolder, "Levels");
            if (Directory.Exists(folderAppData))
            {
                this.levelArray = Directory.GetFiles(folderAppData, "*.lvl").Select(filename => Path.GetFileName(filename)).ToArray();
            }
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            // Simulating slower falling
            this.logic.FindFallingObjects();
        }

        private void TimerForCountDown_Tick(object sender, System.EventArgs e)
        {
            // It makes the game harder because if the time ends the player dies
            if (this.logic.Model.TimeLeft > 1 && this.logic.Model.LivesLeft > 0)
            {
                this.logic.TimeLeft();
            }
            else
            {
                if (this.logic.Model.LivesLeft > 0)
                {
                    this.logic.Died();
                }

                this.ShowMessage();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            // Keystroke analyzer and move function
            bool isOver = false;
            switch (e.Key)
            {
                case Key.Left: isOver = this.logic.Move(-1, 0); break;
                case Key.Right: isOver = this.logic.Move(1, 0); break;
                case Key.Up: isOver = this.logic.Move(0, -1); break;
                case Key.Down: isOver = this.logic.Move(0, 1); break;
            }

            if (isOver)
            {
                this.ShowMessage();
            }
        }

        private void Kill_Me_Mouse_Click(object sender, MouseButtonEventArgs e)
        {
            // If made a mistake we may need to restart the map but we can only do that with suicide
            if (this.logic.Model.LivesLeft > 0 && this.logic.Model.EndEnergy != 100)
            {
                this.logic.Died();
            }

            this.ShowMessage();
        }

        private void ShowMessage()
        {
            // If we pressed the kill me button
            if (this.logic.Model.TimeLeft == 0 || (this.logic.Model.LivesLeft > 0 && this.logic.Model.EndEnergy != 100))
            {
                this.oldLives = this.logic.Model.LivesLeft;
                MessageBox.Show("You died! Try again?");
                this.logic = new GameLogic("Levels/" + this.levelArray[this.currentLevel - 1]);
                this.DataContext = this.logic.Model;
                this.logic.Model.LivesLeft = this.oldLives;
                this.logic.Model.Score = this.score;
                this.logic.Model.CurrentLevel = this.currentLevel;
            } // When we died 3 times
            else if (this.logic.Model.LivesLeft == 0)
            {
                this.stw.Stop();
                this.timerForCountDown.Stop();
                this.logic.Model.Score = this.score;
                MessageBox.Show("Game Over! " + this.stw.Elapsed.Seconds + " seconds" + "\n" + " Score: " + this.logic.Model.Score);
            } // We completed the map => it will load the next one
            else if (this.logic.Model.EndEnergy == 100)
            {
                this.stw.Stop();
                this.timerForCountDown.Stop();
                this.currentLevel++;
                this.score += this.logic.Model.TimeLeft;
                this.logic.Model.Score = this.score;
                MessageBox.Show("Congratulation You made it in " + this.stw.Elapsed.Seconds + " seconds!" + "\n" + " Score: " + this.score);

                // If we ran out of levels we tell it to the user
                if (this.currentLevel > this.levelArray.Count())
                {
                    MessageBox.Show("No more levels sorry :( => Game ended!");
                }
                else
                {
                    this.logic = new GameLogic("Levels/" + this.levelArray[this.currentLevel - 1]);
                    this.DataContext = this.logic.Model;
                    this.stw.Start();
                    this.timerForCountDown.Start();
                    this.logic.Model.Score = this.score;
                    this.logic.Model.CurrentLevel = this.currentLevel;
                }
            }
        }
    }
}
