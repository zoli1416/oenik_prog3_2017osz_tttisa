﻿// <copyright file="PointToGeometryConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Class for converting point to geometry
    /// </summary>
    public class PointToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Function for converting cell coordinate position into pixel coordinate geometry
        /// </summary>
        /// <param name="value"> Point(x,y) position, cell coordinate</param>
        /// <param name="targetType">Type to convert to</param>
        /// <param name="parameter">We dont use it now</param>
        /// <param name="culture">Not relevant</param>
        /// <returns>RectangleGeometry(x,y,w,h), pixel coordinate</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // MultiBinding + IMultiValueConverter
            if (value != null)
            {
                Point p = (Point)value;
                Rect r = new Rect(p.X * GameModel.Tilew, p.Y * GameModel.Tileh, GameModel.Tilew, GameModel.Tileh);
                return new RectangleGeometry(r);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="value">null</param>
        /// <param name="targetType">no tpye</param>
        /// <param name="parameter">no parameter</param>
        /// <param name="culture">not relevant</param>
        /// <returns>no return</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
