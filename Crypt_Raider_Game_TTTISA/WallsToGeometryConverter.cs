﻿// <copyright file="WallsToGeometryConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crypt_Raider_Game_Tttisa
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Class for converting wall to geometry
    /// </summary>
    public class WallsToGeometryConverter : IValueConverter
    {
        // value: bool[,], ahol bool[x,y] == true esetény X,Y cella fal
        // return: GeometryGroup, az összes fallal, pixel koordináta

        /// <summary>
        /// Function for converting walls into pixel coordinate geometrygroup which represents all walls
        /// </summary>
        /// <param name="value">bool[,] where bool[x,y]==true then coordinates points to a wall</param>
        /// <param name="targetType"> We dont use targetType</param>
        /// <param name="parameter"> We dont use parameter</param>
        /// <param name="culture"> We dont use culture</param>
        /// <returns>GeometryGroup, all walls in pixel coordinate</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool[,] walls = (bool[,])value;
            GeometryGroup g = new GeometryGroup();
            for (int x = 0; x < walls.GetLength(0); x++)
            {
                for (int y = 0; y < walls.GetLength(1); y++)
                {
                    if (walls[x, y])
                    {
                        Rect r = new Rect(x * GameModel.Tilew, y * GameModel.Tileh, GameModel.Tilew, GameModel.Tileh);
                        g.Children.Add(new RectangleGeometry(r));
                    }
                }
            }

            return g;
        }

        /// <summary>
        /// We dont use this function
        /// </summary>
        /// <param name="value">No value</param>
        /// <param name="targetType">No targetType</param>
        /// <param name="parameter">No parameter</param>
        /// <param name="culture">No culture</param>
        /// <returns>No return</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
